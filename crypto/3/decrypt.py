#!/usr/bin/env python3

def decrypt(text,key):
	key = list(key)
	text = text.split('#')
	#random.shuffle(key)
	j=0
	for i in range(len(text)):
		text[i] = chr(int(text[i]) - ord(key[j]))
		j = 0 if j>=(len(key)-1) else (j+1)
	string = "".join(text)
	return string

ct = '158#214#218#209#182#235#232#181#164#194#171#239#185#221#191#215#207#211#182#211#232#196#202#225#217#213#180#217#188#205#207#216#227#239'
print (decrypt(ct, "Panetrt"))
