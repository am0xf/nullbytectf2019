from secret import hidden_text
import random

def encrypt(text,key):
	key = list(key)
	text = list(text)
	random.shuffle(key)
	j=0
	for i in range(len(text)):
		text[i] = str(int(ord(text[i])+ord(key[j])))
		j = 0 if j>=(len(key)-1) else (j+1)
	string = "#".join(text)
	return string

print (encrypt(hidden_text,"Pattern"))
